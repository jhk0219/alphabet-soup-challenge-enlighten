import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AlphabetSoup {


    private static int rowCount, columnCount;
    private static final String FILENAME = "/sample.txt";
    //vectors to determine direction of search. [-1, -1] points top left diagonal direction. [0,1] points directly to the right.
    private static int[] x = { -1, -1, -1, 0, 0, 1, 1 ,1};
    private static int[] y = {-1, 0, 1, -1, 1, -1, 0, 1};
    private static int[] startingPoint = new int[2];
    private static int[] endingPoint = new int[2];



    // Given row and column value, search in all available directions
    private static String searchGrid(char[][] grid, int row, int column, String word) {
        String output = "";
        boolean firstCharMatch = false;

        // Check if first char of word matches grid character. If so, update starting position.
        if (grid[row][column] == word.charAt(0)) {
            firstCharMatch = true;
            startingPoint[0] = row;
            startingPoint[1] = column;
        }

        if (firstCharMatch) {
            int wordLength = word.length();

            // Search all directions. If a direction is bound of bounds of the grid, skip.
            for (int searchDirection=0; searchDirection< 8; searchDirection++) {
                int searchedCharacter;
                int rowDir = row + x[searchDirection];
                int colDir = column + y[searchDirection];

                for (searchedCharacter = 1; searchedCharacter < wordLength; searchedCharacter++) {
                    if (rowDir >= rowCount || rowDir < 0 || colDir >= columnCount || colDir < 0) {
                        break;
                    }
                    if (grid[rowDir][colDir] != word.charAt(searchedCharacter)) {
                        break;
                    }

                    //Update ending coordinate once the word has been searched
                    if (searchedCharacter == wordLength-1) {
                        endingPoint[0] = rowDir;
                        endingPoint[1] = colDir;
                        output = word + " " + startingPoint[0] + ":" + startingPoint[1] + " " + endingPoint[0] + ":" + endingPoint[1];
                    }
                    rowDir += x[searchDirection];
                    colDir += y[searchDirection];
                }
            }
        }
        return output;
    }
    private static void findWordPosition(char[][] grid, List<String> words) {

        for (String word: words) {
            for (int row = 0; row < rowCount; row++) {
                for (int col = 0; col < columnCount; col++) {
                    String output = "";
                    output = searchGrid(grid, row, col, word);
                    if (!output.isEmpty()) {
                        System.out.println(output);
                    }
                }
            }
        }


    }

    private static char[][] createGrid(String gridString, int rowCount, int columnCount) {

        char[][] charGrid = new char[rowCount][columnCount];

        int offset =0;
        for (int i=0; i < rowCount ; i++ ) {
            for (int j=0; j < columnCount; j++) {
                charGrid[i][j] = gridString.charAt(offset++);
            }
        }
        return charGrid;
    }


    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> lines;
        lines = Files.readAllLines(Paths.get(AlphabetSoup.class.getResource(FILENAME).toURI()));
        String gridString = "";
        List<String> wordList = new ArrayList<String>();
        for (int i =0; i < lines.size(); i++) {
            // First line to determine grid array size
            if (i ==0) {
                String[] gridSizeLine = lines.get(i).split("x");
                rowCount = Integer.parseInt(gridSizeLine[0]);
                columnCount = Integer.parseInt(gridSizeLine[1]);
                continue;
            }

            //Store grid line info and list of words to find
            if (lines.get(i).contains(" ")) {
                String gridStringWithoutSpace = lines.get(i).replaceAll(" ", "");
                gridString += gridStringWithoutSpace;
            } else {
                wordList.add(lines.get(i));
            }

        }
        char[][] finalGrid  = createGrid(gridString, rowCount, columnCount);
        findWordPosition(finalGrid, wordList);
    }

}
